package Homework;

/**
 *
 *  Let's implement demerit point calculator in traffic
 *
 *      - if a driver speed is equal or smaller than speed limit,
 *      demerit point will be 0
 *
 *      - if a driver speed is greater than speed limit,
 *          demerit points will be calculated by this rule :
 *                 * demerit point will increase 1 point per 5 km
 *
 *      - speed limit is 65
 */

public class DemeritPointsCalculatorHomework {

    public static int calculateDemeritPoint(int driverSpeed, int speedLimit){
        int demeritPoints = 0;
        if (driverSpeed <= speedLimit){
            return demeritPoints;
        }
        if (driverSpeed > speedLimit){
            for(int i = 0; i < (driverSpeed - speedLimit) ;i++){
                if(((driverSpeed - speedLimit) - i) % 5 == 0){
                   demeritPoints++;
                }
            }

        }
        return demeritPoints;
    }

}
