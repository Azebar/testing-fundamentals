package Homework;

public class FactorialNumberCalculatorHomework {


    public static int calculateFactorialNumber(int input) {
        if (input == 0)
            return 1;
        else
            return(input * calculateFactorialNumber(input-1));
    }
}
