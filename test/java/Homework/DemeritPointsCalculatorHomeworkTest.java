package Homework;

import org.junit.Test;

import static org.junit.Assert.*;

public class DemeritPointsCalculatorHomeworkTest {

    @Test
    public void testDemeritPointsCalculatorDriverSpeed70SpeedLimit65Result1(){
        int driverSpeed = 70;
        int speedLimit = 65;
        int expected = 1;

        int result = DemeritPointsCalculatorHomework.calculateDemeritPoint(driverSpeed, speedLimit);

        assertEquals(expected, result);
    }
    @Test
    public void testDemeritPointsCalculatorDriverSpeed75SpeedLimit65Result2(){
        int driverSpeed = 75;
        int speedLimit = 65;
        int expected = 2;

        int result = DemeritPointsCalculatorHomework.calculateDemeritPoint(driverSpeed, speedLimit);

        assertEquals(expected, result);
    }
    @Test
    public void testDemeritPointsCalculatorDriverSpeed84SpeedLimit65Result3(){
        int driverSpeed = 84;
        int speedLimit = 65;
        int expected = 3;

        int result = DemeritPointsCalculatorHomework.calculateDemeritPoint(driverSpeed, speedLimit);

        assertEquals(expected, result);
    }
    @Test
    public void testDemeritPointsCalculatorDriverSpeed60SpeedLimit65Result0(){
        int driverSpeed = 60;
        int speedLimit = 65;
        int expected = 0;

        int result = DemeritPointsCalculatorHomework.calculateDemeritPoint(driverSpeed, speedLimit);

        assertEquals(expected, result);
    }
    @Test
    public void testDemeritPointsCalculatorDriverSpeed65SpeedLimit65Result0(){
        int driverSpeed = 65;
        int speedLimit = 65;
        int expected = 0;

        int result = DemeritPointsCalculatorHomework.calculateDemeritPoint(driverSpeed, speedLimit);

        assertEquals(expected, result);
    }
    @Test
    public void testDemeritPointsCalculatorDriverSpeed67SpeedLimit65Result0(){
        int driverSpeed = 67;
        int speedLimit = 65;
        int expected = 0;

        int result = DemeritPointsCalculatorHomework.calculateDemeritPoint(driverSpeed, speedLimit);

        assertEquals(expected, result);
    }

}