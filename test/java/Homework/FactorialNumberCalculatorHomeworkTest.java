package Homework;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 *
 * Try to implement a method that calculates
 * factorial number for given input by applying
 * TDD
 *
 */

public class FactorialNumberCalculatorHomeworkTest {

    @Test
    public void testCalculateFactorialNumberInput0Result1(){
        int input = 0;
        int expected = 1;

        int result = FactorialNumberCalculatorHomework.calculateFactorialNumber(input);

        assertEquals(expected, result);
    }

    @Test
    public void testCalculateFactorialNumberInput1Result1(){
        int input = 1;
        int expected = 1;

        int result = FactorialNumberCalculatorHomework.calculateFactorialNumber(input);

        assertEquals(expected, result);
    }

    @Test
    public void testCalculateFactorialNumberInput2Result2(){
        int input = 2;
        int expected = 2;

        int result = FactorialNumberCalculatorHomework.calculateFactorialNumber(input);

        assertEquals(expected, result);
    }

    @Test
    public void testCalculateFactorialNumberInput3Result6(){
        int input = 3;
        int expected = 6;

        int result = FactorialNumberCalculatorHomework.calculateFactorialNumber(input);

        assertEquals(expected, result);
    }

}
