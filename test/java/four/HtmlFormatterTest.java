package four;

import org.junit.Test;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import static org.assertj.core.api.Java6Assertions.assertThat;
import static org.assertj.core.api.Java6Assertions.entry;
import static org.junit.Assert.*;

public class HtmlFormatterTest {

    /**
     * Assertj
     *
     * doc : https://assertj.github.io/doc/
     */

    @Test
    public void testFormatAsBold_WhenCalled_ShouldEncloseTheStringWithStrongElement()
    {
        HtmlFormatter formatter = new HtmlFormatter();
        String content = "abc";
        String result = formatter.formatAsBold(content);

        assertThat(result).isEqualToIgnoringCase("<strong>abc</strong>");

        assertThat(result).startsWith("<strong>");
        assertThat(result).endsWith("</strong>");
        assertThat(result).contains(content);
    }


    @Test
    public void testAssertThatWithFile() {
        assertThat(new File("C:\\Users\\fferi\\OneDrive" +
                "\\Desktop\\http_tools.txt"))
                .exists()
                .isFile()
                .canRead()
                .canWrite();
    }
    @Test
    public void testAssertThatWithMap() {
        Map<Integer,String> map = new HashMap<Integer, String>();
        map.put(2,"a");
        assertThat(map)
                .isNotEmpty()
                .containsKey(2)
                .doesNotContainKeys(10)
                .contains(entry(2, "a"));
    }

}