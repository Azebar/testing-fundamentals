package two;

import org.junit.Test;

import static org.junit.Assert.*;

public class ReservationTest {

    @Test
    public void testCanBeCancelledByAdminCancellingReturnsTrue(){

        Reservation reservation = new Reservation();
        User adminUser = new User("Feride", "Celik",true);

        boolean result = reservation.canBeCancelledBy(adminUser);

        assertEquals(true, result);
        assertTrue(result);
    }

    @Test
    public void testCanBeCancelledBySameUserCancellingReturnsTrue(){

        User user = new User("Feride", "Celik",false);
        Reservation reservation = new Reservation(user);

        boolean result = reservation.canBeCancelledBy(user);

        assertEquals(true, result);
        assertTrue(result);
    }

    @Test
    public void testCanBeCancelledByAnotherUserCancellingReturnsFalse(){

        User madeBy = new User("Feride", "Celik", false);
        User anotherUser = new User("Other", "Other person", false);
        Reservation reservation = new Reservation(madeBy);

        boolean result = reservation.canBeCancelledBy(anotherUser);

        assertEquals(false, result);
        assertFalse(result);

    }

}